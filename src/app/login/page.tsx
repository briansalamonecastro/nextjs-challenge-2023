"use client";
import React, { useState } from "react";
import { TextField, Button } from "@radix-ui/themes";
import { useDispatch } from "react-redux";
import { login } from "../../redux/slices/authSlice";
import { useRouter } from "next/navigation";
import { object, string, InferType } from "yup";
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import { yupResolver } from "@hookform/resolvers/yup";

let userSchema = object({
  username: string()
    .required("Username is required.")
    .test("is-lowercase", "The username does not exist.", (value) =>
      /^[a-z]*$/.test(value)
    ),
  password: string()
    .required("Password is required.")
    .test(
      "is-123Username",
      "Incorrect password.",
      (value, { parent: { username } }) => {
        const capitalizedUsername =
          username.charAt(0).toUpperCase() + username.slice(1);

        const validPassword =
          value.startsWith("123") && value.slice(3) === capitalizedUsername;
        return validPassword;
      }
    ),
});

type User = InferType<typeof userSchema>;

export default function LoginPage() {
  const dispatch = useDispatch();
  const { push } = useRouter();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<User>({
    resolver: yupResolver(userSchema),
  });

  const handleLogin = async (user: any) => {
    try {
      const parsedUser = await userSchema.validate(user);

      dispatch(login(parsedUser.username));
      push("/");
    } catch (err: any) {
      console.error(err);
    }
  };

  return (
    <div className="w-full sm:max-w-sm mx-auto p-10 bg-gray-200 rounded-md m-10">
      <h1 className="font-bold text-xl">Login</h1>
      <form onSubmit={handleSubmit(handleLogin)} className="[&>div]:my-3">
        <div>
          <label>Username</label>
          <TextField.Input type="text" {...register("username")} />
          <ErrorMessage
            errors={errors}
            name="username"
            render={({ message }) => message && <p>{message}</p>}
          />
        </div>
        <div>
          <label>Password</label>
          <TextField.Input type="password" {...register("password")} />
          <ErrorMessage
            errors={errors}
            name="password"
            render={({ message }) => message && <p>{message}</p>}
          />
        </div>
        <Button type="submit">Login</Button>
      </form>
    </div>
  );
}

LoginPage.getLayout = (page: any) => page;
