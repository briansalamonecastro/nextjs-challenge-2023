"use client";
import HamburgerMenuButton from "@/components/HamburgerMenuButton";
import Sidebar from "@/components/SideBar";
import { useState } from "react";

export default function NavLayout({ children }: { children: React.ReactNode }) {
  return (
    <div className="flex flex-col h-full">
      <header className="h-10 shadow-md flex">
        <HamburgerMenuButton className="m-2" />
      </header>
      <main className="flex flex-grow items-stretch">
        <Sidebar />
        <article className="px-10 py-5 flex-grow">{children}</article>
      </main>
    </div>
  );
}
