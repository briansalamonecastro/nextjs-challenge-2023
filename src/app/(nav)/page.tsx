"use client";
import ImageList from "@/components/ImageList";
import { picsumApi, useGetPicturesQuery } from "@/redux/services/picsumApi";
import { RootState } from "@/redux/store";
import { Button } from "@radix-ui/themes";
import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const ITEMS_PER_PAGE = 6;

export default function HomePage() {
  const [page, setPage] = useState(1);
  const {
    data: pictures,
    error,
    isLoading,
  } = useGetPicturesQuery({ page, limit: ITEMS_PER_PAGE });

  const { savedImageIds } = useSelector(({ auth }: RootState) => auth);

  const dispatch = useDispatch();

  const loadMore = useCallback((_lastIndex: number) => {
    setPage((prev) => {
      console.log("cargon pagina " + prev);
      return prev + 1;
    });
  }, []);

  return isLoading ? (
    <>Loading...</>
  ) : (
    <div className="flex flex-col h-full w-full">
      <Button
        onClick={() => {
          dispatch(picsumApi.util.resetApiState());
          setPage(1);
        }}
      >
        Refresh
      </Button>
      {pictures && (
        <ImageList
          data={pictures}
          loadMore={loadMore}
          savedImageIds={savedImageIds!}
        />
      )}
    </div>
  );
}
