"use client";
import ImageList from "@/components/ImageList";
import { RootState } from "@/redux/store";
import React from "react";
import { useSelector } from "react-redux";

const Gallery = () => {
  const { savedImageIds } = useSelector(({ auth }: RootState) => auth);

  return (
    <div className="flex flex-col">
      <h1>Image Gallery</h1>
      <div className="flex flex-col h-full w-full">
        {savedImageIds?.length && (
          <ImageList
            data={savedImageIds.map((id, index) => ({ id } as PicsumThumbnail))}
            savedImageIds={savedImageIds}
          />
        )}
      </div>
    </div>
  );
};

export default Gallery;
