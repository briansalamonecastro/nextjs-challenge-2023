import { Button } from "@radix-ui/themes";
import Link from "next/link";
import React from "react";

export default function ProfilePage() {
  return (
    <div>
      <h1>ProfilePage</h1>
      <br />
      <Button variant="classic">
        <Link href="/profile/edit">Edit</Link>
      </Button>
    </div>
  );
}
