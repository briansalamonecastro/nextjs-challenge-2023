"use client";
import React, { ChangeEvent, useState } from "react";
import * as AspectRatio from "@radix-ui/react-aspect-ratio";
import { Button, TextField } from "@radix-ui/themes";
import Image from "next/image";
import { useForm } from "react-hook-form";

//https://picsum.photos/200

export default function ProfileEditScreen() {
  const [image, setImage] = useState<File>();
  const [createObjectURL, setCreateObjectURL] = useState("");
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  // Define your gallery data here (e.g., images)
  const galleryData: string[] = [
    // "image1.jpg",
    // "image2.jpg",
    // "image3.jpg",
    // Add more image URLs as needed
  ];

  const onSubmit = async ({ images }: any) => {
    const image = images[0];
    if (!image) return;
    const body = new FormData();
    body.append("file", image);
    let res = await fetch(`http://localhost:3000/api/upload`, {
      method: "POST",
      body,
    });
    let response = await res.json();
  };

  const handleChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    if (target.files && target.files[0]) {
      const i = target.files[0];

      setImage(i);
      setCreateObjectURL(URL.createObjectURL(i));
    }
  };

  return (
    <>
      <h2>Upload a profile picture</h2>
      <form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
        <div className="w-[200px]">
          <TextField.Input
            type="file"
            accept="image/*"
            multiple={false}
            {...register("images", { onChange: handleChange })}
          />
          {image && (
            <AspectRatio.Root ratio={1}>
              <Image alt="Image to upload" src={createObjectURL} fill />
            </AspectRatio.Root>
          )}
        </div>
        <Button type="submit">Upload</Button>
      </form>
      <div className="gallery">
        {galleryData.map((image, index) => (
          <AspectRatio.Root ratio={1} key={index}>
            <Image src={image} alt={`${index + 1}`} fill />
          </AspectRatio.Root>
        ))}
      </div>
    </>
  );
}
