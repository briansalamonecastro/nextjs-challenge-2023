"use client";

import { ReactNode } from "react";
import { store } from "./redux/store";
import { Provider } from "react-redux";
import { Theme } from "@radix-ui/themes";
import Auth from "./components/Auth";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

export const persistor = persistStore(store);

export function Providers({ children }: { children: ReactNode }) {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Auth>
          <Theme className="h-full">{children}</Theme>
        </Auth>
      </PersistGate>
    </Provider>
  );
}
