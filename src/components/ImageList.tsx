import { VirtuosoGrid } from "react-virtuoso";
import React, { LegacyRef, useEffect, useRef } from "react";
import PicsumImage from "./PicsumImage";

const ItemWrapper = React.forwardRef((props: any, ref) => (
  <div
    ref={ref as LegacyRef<HTMLDivElement>}
    {...props}
    className="relative w-full md:w-1/2 lg:w-1/3"
    style={{ aspectRatio: 4 / 3 }}
  >
    {props.children}
  </div>
));
ItemWrapper.displayName = "ItemWrapper";

const ListWrapper = React.forwardRef((props: any, ref) => (
  <div
    ref={ref as LegacyRef<HTMLDivElement>}
    {...props}
    className="flex flex-wrap"
  >
    {props.children}
  </div>
));
ListWrapper.displayName = "ListWrapper";

type ImageListProps = {
  data: PicsumThumbnail[];
  loadMore?: (_lastIndex: number) => void;
  savedImageIds: number[];
};

export default function ImageList({
  data,
  loadMore,
  savedImageIds,
}: ImageListProps) {
  const isLoading = useRef(true);

  useEffect(() => {
    if (isLoading.current && data?.length && loadMore) {
      loadMore!(0);
      isLoading.current = false;
    }
  }, [data, loadMore]);

  return (
    data && (
      <VirtuosoGrid
        style={{ height: "100%" }}
        useWindowScroll
        overscan={600}
        data={data}
        endReached={loadMore}
        components={{
          // Footer: () => <span>Loading...</span>,
          Item: ItemWrapper,
          List: ListWrapper,
          ScrollSeekPlaceholder: ({ height, width, index }) => (
            <ItemWrapper>
              <p>--</p>
            </ItemWrapper>
          ),
        }}
        scrollSeekConfiguration={{
          enter: (velocity) => Math.abs(velocity) > 1000,
          exit: (velocity) => Math.abs(velocity) < 30,
          // change: (_, range) => console.log({ range }),
        }}
        itemContent={(index, item) => {
          if (!item) {
            return <div>Blank</div>;
          }
          const { id } = item;
          const isSaved = !!(savedImageIds && savedImageIds.indexOf(id) + 1);
          return <PicsumImage id={id} isSaved={isSaved} />;
        }}
      />
    )
  );
}
