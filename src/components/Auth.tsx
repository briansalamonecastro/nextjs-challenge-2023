"use client";

import { usePathname } from "next/navigation";
import { ReactNode } from "react";
import { useSelector } from "react-redux";
import { redirect } from "next/navigation";
import { RootState } from "@/redux/store";

const unprotectedRoutes = ["/login"];
const regexpUnprotectedRoutes = new RegExp(`^(${unprotectedRoutes.join("|")})`);

export default function Auth({ children }: { children: ReactNode }) {
  const { isAuthenticated } = useSelector(({ auth }: RootState) => auth);
  const pathname = usePathname();

  if (!regexpUnprotectedRoutes.test(pathname) && !isAuthenticated) {
    redirect("/login");
  }

  return children;
}
