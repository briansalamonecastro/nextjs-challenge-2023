"use client";
import Styles from "./SideBar.module.css";
import React from "react";
import * as NavigationMenu from "@radix-ui/react-navigation-menu";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "@/redux/slices/authSlice";
import { Button, Separator } from "@radix-ui/themes";
import { HomeIcon, ImageIcon } from "@radix-ui/react-icons";
import { RootState } from "@/redux/store";

const Sidebar = () => {
  const { isMenuOpen } = useSelector(({ auth }: RootState) => auth);
  const dispatch = useDispatch();
  const sidebarConditionalClasses = isMenuOpen ? "" : "-ml-80";

  return (
    <div
      className={`flex flex-col h-full w-full sm:w-80 bg-gray-200 duration-300 ${sidebarConditionalClasses}`}
    >
      <div className="h-screen">
        <NavigationMenu.Root orientation="vertical" className="flex-grow h-4/6">
          <NavigationMenu.List className={Styles.list}>
            <Link href="/" className={Styles.item}>
              <NavigationMenu.Item>
                <NavigationMenu.Trigger className="flex items-center">
                  <HomeIcon className="mx-2" />
                  Home
                </NavigationMenu.Trigger>
              </NavigationMenu.Item>
            </Link>

            <Link href="/my-images" className={Styles.item}>
              <NavigationMenu.Item>
                <NavigationMenu.Trigger className="flex items-center">
                  <ImageIcon className="mx-2" />
                  My images
                </NavigationMenu.Trigger>
              </NavigationMenu.Item>
            </Link>

            <NavigationMenu.Indicator />
          </NavigationMenu.List>

          <NavigationMenu.Viewport />
        </NavigationMenu.Root>

        <footer className={Styles["sidebar-footer"]}>
          <hr className="border-gray-300 m-5" />
          <Link href="/profile" className={`${Styles.item} block my-2`}>
            Profile
          </Link>

          <Button
            className={`${Styles.button} self-baseline`}
            variant="soft"
            onClick={() => dispatch(logout())}
          >
            Logout
          </Button>
        </footer>
      </div>
    </div>
  );
};

export default Sidebar;
