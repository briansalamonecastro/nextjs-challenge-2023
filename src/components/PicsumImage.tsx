import { useGetPictureByIdQuery } from "@/redux/services/picsumApi";
import { BookmarkFilledIcon, BookmarkIcon } from "@radix-ui/react-icons";
import { saveImage, unsaveImage } from "@/redux/slices/authSlice";
import { Button } from "@radix-ui/themes";
import Image from "next/image";
import React from "react";
import { useDispatch } from "react-redux";

type PicsumImageProps = {
  id: number;
  isSaved?: boolean;
};

export default function PicsumImage({ id, isSaved }: PicsumImageProps) {
  const { data: picsumThumbnail, isLoading } = useGetPictureByIdQuery(id);
  const dispatch = useDispatch();

  const { url, author } = picsumThumbnail || {};

  return isLoading ? (
    <>Loading...</>
  ) : (
    <div className="h-full m-10 flex flex-col rounded-b shadow">
      <div className="h-5/6 relative">
        <Image
          key={id}
          fill
          className="object-cover rounded-t"
          sizes="(max—width: 768px) 100vw, (max—width: 1200px) 50vw, 33vw"
          alt={String(id)}
          src={url}
        />
      </div>
      <div className="h-1/6 px-5 py-1 flex items-center justify-between">
        <p className="text-gray-400">
          Author <span className="text-gray-800">{author}</span>
        </p>
        <Button
          variant="ghost"
          className="!rounded-full ![&>*]:rounded-full !cursor-pointer"
          style={{ padding: "10px" }}
          onClick={() => {
            dispatch(isSaved ? unsaveImage(id) : saveImage(id));
          }}
        >
          {isSaved ? <BookmarkFilledIcon /> : <BookmarkIcon />}
        </Button>
      </div>
    </div>
  );
}
