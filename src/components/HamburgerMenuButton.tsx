"use client";
import { toggleMenu } from "@/redux/slices/authSlice";
import { RootState } from "@/redux/store";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

type HamburgerMenuButtonProps = {
  className: string;
};

const HamburgerMenuButton = ({ ...props }: HamburgerMenuButtonProps) => {
  const { isMenuOpen } = useSelector(({ auth }: RootState) => auth);
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(toggleMenu());
  };

  return (
    <span {...props}>
      <button
        onClick={handleClick}
        className="flex flex-col justify-center h-6 w-6 items-center"
      >
        <span
          className={`bg-amber-700 block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm ${
                      isMenuOpen
                        ? "rotate-45 translate-y-1"
                        : "-translate-y-0.5"
                    }`}
        ></span>
        <span
          className={`bg-amber-700 block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm my-0.5 ${
                      isMenuOpen ? "opacity-0" : "opacity-100"
                    }`}
        ></span>
        <span
          className={`bg-amber-700 block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm ${
                      isMenuOpen
                        ? "-rotate-45 -translate-y-1"
                        : "translate-y-0.5"
                    }`}
        ></span>
      </button>
    </span>
  );
};
export default HamburgerMenuButton;
