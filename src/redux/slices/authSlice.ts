import { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import { NextApiResponse } from "next";

export interface AuthState {
  username?: string;
  isAuthenticated?: Boolean;
  savedImageIds?: number[];
  isMenuOpen?: boolean;
}

const initialState: AuthState = {};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    saveImage: (state, action: PayloadAction<number>) => {
      if (!state.savedImageIds) {
        state.savedImageIds = [];
      }
      state.savedImageIds = [...state.savedImageIds, action.payload];
    },
    unsaveImage: (state, action: PayloadAction<number>) => {
      if (!state.savedImageIds) return;

      const i = state.savedImageIds.indexOf(action.payload);
      if (i + 1) state.savedImageIds.splice(i, 1);

      if (!state.savedImageIds.length) delete state.savedImageIds;
    },
    login: (state, action: PayloadAction<string>) => {
      state.username = action.payload;
      state.isAuthenticated = true;
    },
    logout: (state) => {
      return initialState;
    },
    toggleMenu: (state, action: PayloadAction<boolean | undefined>) => {
      if (action.payload) {
        state.isMenuOpen = action.payload;
      } else {
        state.isMenuOpen = !state.isMenuOpen;
      }
    },
  },
});

// Action creators are generated for each case reducer function
export const { login, logout, saveImage, unsaveImage, toggleMenu } =
  authSlice.actions;

export default authSlice.reducer;
