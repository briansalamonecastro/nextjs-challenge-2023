import { MaybePromise } from "@reduxjs/toolkit/dist/query/tsHelpers";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const BASE_URL = "https://picsum.photos";

async function tryMaybePromise(maybePromise: MaybePromise<any>) {
  const response = await maybePromise;
  if (response.error) throw response.error;
  return response.data;
}

// Define a service using a base URL and expected endpoints
export const picsumApi = createApi({
  reducerPath: "picsumApi",
  baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),
  endpoints: (builder) => ({
    getPictures: builder.query<
      PicsumThumbnail[],
      { page?: number; limit?: number }
    >({
      queryFn: async ({ page, limit }, _api, _opt, fetchWithBQ) => {
        const picsumThumbnails = (await tryMaybePromise(
          fetchWithBQ({
            url: "v2/list",
            params: { page, limit },
          })
        )) as PicsumThumbnail[];

        return { data: picsumThumbnails };
      },
      serializeQueryArgs: ({ queryArgs }) => {
        const { page, ...otherArgs } = queryArgs;
        return otherArgs;
      },
      merge: (currentCache, newItems) => {
        currentCache.push(...newItems);
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg?.page !== previousArg?.page;
      },
    }),
    getPictureById: builder.query<PicsumThumbnail, number>({
      queryFn: async (id, _api, _opt, fetchWithBQ) => {
        const picsumThumbnail = (await tryMaybePromise(
          fetchWithBQ({
            url: `id/${id}/info`,
          })
        )) as PicsumThumbnail;

        picsumThumbnail.url = BASE_URL + `/id/${id}/800/800`;
        return { data: picsumThumbnail };
      },
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetPicturesQuery, useGetPictureByIdQuery } = picsumApi;
